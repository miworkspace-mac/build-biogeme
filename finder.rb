#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

download_page = "http://biogeme.epfl.ch/home.html#"

begin
  doc = Nokogiri::HTML(open(download_page,
  	  "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 1074) AppleWebKit/536.25 (KHTML like Gecko) Version/6.0 Safari/536.25") )
  rescue
    puts "Failed to fetch download page"
    exit 1
  end

link = doc.xpath('//a').find {|x| x.inner_html.match(/OSX/) }

if link
  puts URI::join(download_page, link["href"])
else
  exit -1
end